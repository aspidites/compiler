{-# LANGUAGE OverloadedStrings #-}

module Main where

import Text.Megaparsec
import Text.Megaparsec.Char
import qualified Data.Text as T
import qualified Data.Text.IO as T
import Control.Monad
import Data.Text (Text)
import Data.Void
import System.IO
import Data.Maybe

import Interpreter
import Grammar
import Parser

repl :: IO ()
repl = do
  putStr "> "
  hFlush stdout
  getLine >>= interpret . T.pack >>= print
  main

main :: IO ()
main = test

printPartial :: Text -> IO ()
printPartial s = do
  putStrLn ""
  case parseExpr s of
    Nothing -> putStr "Invalid expression"
    Just e -> do
      putStrLn $ "partial evaluator: " ++ show e ++ " => " ++ show (partialEvaluator e)
      unoptimized <- interpretExpr e
      optimized <- interpretExpr (partialEvaluator e)
      putStrLn $ show unoptimized ++ " => " ++ show optimized


test :: IO ()
test = 
  mapM_ printPartial
    [ "(+ (read) (- (+ 5 3)))"
    , "(+ 1 (+ (read) 1))"
    , "(- (+ (read) (- 5)))"
    ]
