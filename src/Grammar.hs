module Grammar where

data Expr
  = ExInt Int
  | ExRead
  | ExNegate Expr
  | ExAdd Expr Expr

instance Show Expr where
  show (ExInt x) = show x
  show ExRead = "(read)"
  show (ExNegate e) = "(- " ++ show e ++ ")"
  show (ExAdd e f) = "(+ " ++ show e ++ " " ++ show f ++ ")"

newtype Program 
  = R0 { getExpr :: Expr }

instance Show Program where
  show (R0 expr) = "'(program " ++ show expr ++ ")"

leaf :: Expr -> Bool
leaf ExRead = True
leaf (ExInt _) = True
leaf _ = False
