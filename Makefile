.PHONY: watch
watch:
	ghcid -c 'cabal new-repl' --reload=./Main.hs -T Main.test --restart=./compiler.cabal

.PHONY: repl
repl: 
	cabal v2-run
