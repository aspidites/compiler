{-# LANGUAGE OverloadedStrings #-}
module Interpreter where

import Grammar
import Parser
import Data.Text (Text)
import Data.Maybe
import System.IO

interpretExpr :: Expr -> IO Int
interpretExpr expr
  = case expr of
      (ExInt i) -> return i
      ExRead -> do
        putStr ">> "
        hFlush stdout
        readLn :: IO Int
      (ExNegate e) -> negate <$> interpretExpr e
      (ExAdd e f) -> (+) <$> interpretExpr e <*> interpretExpr f

interpretP :: (Text -> Maybe a) -> (a -> Expr) -> Text -> IO Int
interpretP p f 
  = maybe 
    (error "Invalid program") 
    (interpretExpr . partialEvaluator . f)
  . p

interpretR0 :: Text -> IO Int
interpretR0 = interpretP parseR0 getExpr

interpret :: Text -> IO Int
interpret = interpretP parseExpr id
