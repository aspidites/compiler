{-# LANGUAGE OverloadedStrings #-}
module Parser where

import Control.Monad (void)
import Data.Void
import Data.Text (Text)
import Data.Functor
import Data.Maybe

import Text.Megaparsec
import Text.Megaparsec.Debug
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L

import Grammar

type Parser = Parsec Void Text

-- Generic Parsers

sc :: Parser ()
sc = L.space space1 (L.skipLineComment ";") (L.skipBlockComment "#|" "|#")

lexeme :: Parser a -> Parser a
lexeme = L.lexeme sc

symbol :: Text -> Parser Text
symbol = L.symbol sc

testP :: (Show b) => Text -> Parser b -> IO ()
testP = flip parseTest


sExpr :: Text -> Parser a -> Parser a
sExpr s p = between (symbol "(") (symbol ")") $ symbol s >> p

-- Grammar Parsers

newline :: Parser Char
newline = single '\n'

parseR0 :: Text -> Maybe Program
parseR0 = parseMaybe programP

parseExpr :: Text -> Maybe Expr
parseExpr = parseMaybe exprP

isR0 :: Text -> Bool
isR0 = isJust . parseR0

programP :: Parser Program
programP = do
  symbol "'"
  R0 <$> sExpr "program" exprP

exprP :: Parser Expr
exprP = try intP <|> try readP <|> try negateP <|> try addP

intP :: Parser Expr
intP = ExInt <$> lexeme L.decimal

readP :: Parser Expr
readP = sExpr "read" $ return ExRead

negateP :: Parser Expr
negateP = sExpr "-" $ ExNegate <$> exprP

addP :: Parser Expr
addP = sExpr "+" $ ExAdd <$> exprP <*> exprP

-- Optimizations
partialEvaluator :: Expr -> Expr
partialEvaluator expr 
  = case expr of
      (ExAdd (ExInt x) (ExAdd (ExInt y) e)) -> partialEvaluator $ ExAdd (ExInt (x + y)) (partialEvaluator e)
      (ExAdd (ExInt x) (ExAdd e (ExInt y))) -> partialEvaluator $ ExAdd (ExInt (x + y)) (partialEvaluator e)
      (ExAdd (ExInt x) (ExInt y)) -> ExInt (x + y)
      (ExNegate (ExAdd e (ExNegate (ExInt x)))) -> ExNegate (ExAdd (partialEvaluator e) (ExInt $ negate x))
      (ExNegate (ExAdd (ExInt x) (ExInt y))) -> ExInt (negate (x + y))
      (ExAdd e neg@(ExNegate f)) -> partialEvaluator $ ExAdd e (partialEvaluator neg)
      e -> e
